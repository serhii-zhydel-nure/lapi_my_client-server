﻿namespace Network.Messages
{
  public abstract class AHostMessage
  {
    public readonly MessageCode Code;

    protected AHostMessage(MessageCode messageType)
    {
      Code = messageType;
    }

    public abstract byte[] Serialize();


    public abstract void DeSerialize(byte[] messageData);
  }
}