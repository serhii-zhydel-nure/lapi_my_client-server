﻿using System.Collections.Generic;

namespace Network.Messages
{

  public enum MessageCode:short
    //32767 possible values
  {
    RequestClientName
  }




  /// <summary>
  /// Optimizing boxing when comparing MessageCode's
  /// </summary>
  public struct MessageCodeComparer : IEqualityComparer<MessageCode>
  {
    public bool Equals(MessageCode x, MessageCode y)
    {
      return x == y;
    }

    public int GetHashCode(MessageCode obj)
    {
      // you need to do some thinking here,
      return (int)obj;
    }
  }



  public static class MessageCodeExtender
  {
    public static short ToShort(this MessageCode code)
    {
      return (short) code;
    }
  }
}