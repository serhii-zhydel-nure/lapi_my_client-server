﻿using System;
using System.Collections.Generic;
using Network.Messages;

namespace Network
{
  /// <summary>
  /// По первым байтам сообщения определяет, кому отдать его обработку
  /// </summary>
  public class MessageDistributor
  {
    private AHost _messageProvider;

    private Dictionary<MessageCode, Action<MessageData>> _messageHandlers;


    public void SetMessageProvider(AHost host)
    {
      if (_messageProvider != null)
        _messageProvider.DataMessageReceived -= DistributeMessage;

      _messageProvider = host;

      _messageHandlers = new Dictionary<MessageCode, Action<MessageData>>(new MessageCodeComparer());

      _messageProvider.DataMessageReceived += DistributeMessage;
    }

    private void DistributeMessage(MessageData message)
    {
      throw new NotImplementedException();
    }

    public void AddMessageHandler(MessageCode messageType, Action<MessageData> handler)
    {
      if (!_messageHandlers.ContainsKey(messageType))
        _messageHandlers.Add(messageType, handler);
      else
        _messageHandlers[messageType] += handler;
    }

    public void RemoveMessageHandler(MessageCode messageType, Action<MessageData> handler)
    {
      if (!_messageHandlers.ContainsKey(messageType))
        throw new ArgumentException("No such supported messageCode handlers.");

      _messageHandlers[messageType] -= handler;
    }
  }
}