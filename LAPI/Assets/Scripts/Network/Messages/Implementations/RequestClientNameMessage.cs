﻿using UnityEngine.Networking;

namespace Network.Messages.Implementations
{
  public class RequestClientNameMessage:AHostMessage
  {
    public RequestClientNameMessage() : base(MessageCode.RequestClientName)
    {
    }

    public override void DeSerialize(byte[] messageData)
    {
      NetworkWriter writer = new NetworkWriter();
      writer.StartMessage(Code.ToShort());
    }

    public override byte[] Serialize()
    {
      throw new System.NotImplementedException();
    }
  }
}