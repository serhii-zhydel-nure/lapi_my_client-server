﻿using System;
using UnityEngine.Networking;

namespace Network
{
  /// <summary>
  /// Data 
  /// </summary>
  public struct MessageData
  {
    /// <summary>
    /// Id of udp socket where event happened
    /// </summary>
    public int HostId;

    /// <summary>
    /// Where it come from
    /// </summary>
    public int ConnectionId;

    /// <summary>
    /// By which channel
    /// </summary>
    public int ChannelId;

    /// <summary>
    /// Where to save data, and how many
    /// </summary>
    public byte[] Buffer;


    public NetworkError Error;


    public MessageData(int bufferSize) : this()
    {
      Buffer = new byte[bufferSize];
    }

    public override string ToString()
    {
      return String.Format(
        "HostId: {0} ConnectionId:{1} ChannelId:{2} Buffer:{3} Error:{4}",
        HostId, ConnectionId, ChannelId, Buffer, Error);
    }
  }
}