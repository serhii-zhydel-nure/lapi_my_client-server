﻿using System;
using UnityEngine;

namespace Network
{
  [RequireComponent(typeof(Server))]
  public class ServerView: MonoBehaviour
  {
    private Server _server;

    private void Awake()
    {
      _server = GetComponent<Server>();
    }

    void OnGUI()
    {
      float lineHeight =Screen.height * 0.05f;

      Rect lableRect =
        new Rect(Screen.width * 0.03f,
          Screen.height * 0.03f,
          Screen.width * 0.1f,
          Screen.height * 0.05f);

      Rect areaRect =
        new Rect(Screen.width * 0.07f,
          Screen.height * 0.03f,
          Screen.width * 0.07f,
          Screen.height * 0.05f);

      if (!_server.IsHostStarted)
      {
        //ip
        GUI.Label(lableRect, "Ip");
        _server.Config.IpAddress = 
          GUI.TextField(areaRect, _server.Config.IpAddress);

        lableRect.y += lineHeight;

        areaRect.y += lineHeight;
        //port
        GUI.Label(lableRect, "Port");
        _server.Config.Port = Int32.Parse(
          GUI.TextField(areaRect, _server.Config.Port.ToString()));

        lableRect.y += lineHeight;
        //button
        if (GUI.Button(lableRect, "Start server"))
        {
          _server.StartServer();
        }
      }

      else
      {
        lableRect.width += Screen.width * 0.1f;
        GUI.Label(lableRect, 
          String.Format("Runing [{0}:{1}]", 
            _server.Config.IpAddress, _server.Config.Port));
        lableRect.width -= Screen.width * 0.1f;

        lableRect.y += lineHeight;
        if(GUI.Button(lableRect, "Stop server"))
        {
          _server.StopServer();
        }
      }
    }

  }
}