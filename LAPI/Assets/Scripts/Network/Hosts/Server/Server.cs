﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
  public class Server : AHost
  {
    public int WebHostId { get { return _webHostId; } }
    [SerializeField] [ReadOnly] private int _webHostId;

    [SerializeField]
    [ReadOnly]
    private List<int> _connections = new List<int>();


    /// <summary>
    /// int - connectionID
    /// </summary>
    public event Action<int> ClientConnected;

    /// <summary>
    /// int - connectionID
    /// </summary>
    public event Action<int> ClientDisconnected;

    public void StartServer()
    {
      if (!IsHostStarted)
      {
        StartHost();
      }

    }

    protected override void AddHost(HostTopology topology)
    {
      //null - чтоб все кто хочет могли подключаться
      HostId = NetworkTransport.AddHost(topology, Config.Port, null);
      _webHostId = NetworkTransport.AddWebsocketHost(topology, Config.Port, null);
    }

    protected override void RemoveHost()
    {
      NetworkTransport.RemoveHost(HostId);
      NetworkTransport.RemoveHost(_webHostId);
    }

    public void StopServer()
    {
      StopHost();
    }

    protected override void OnConnect(MessageData messageData)
    {
      Debug.LogFormat("Client {0} connected.", messageData.ConnectionId);
      _connections.Add(messageData.ConnectionId);

      if (ClientConnected != null)
        ClientConnected.Invoke(messageData.ConnectionId);
    }

    protected override void OnDisconnect(MessageData messageData)
    {
      Debug.LogFormat("Client {0} disconnected.", messageData.ConnectionId);
      _connections.Remove(messageData.ConnectionId);

      if (ClientDisconnected != null)
        ClientDisconnected.Invoke(messageData.ConnectionId);
    }

    //public void SendMessage

    //public void SendMessage(int connectionId, int channelId, byte[] message)
    //{
    //  NetworkTransport.Send(_hostId, connectionId, channelId, message, message.Length, out _error);
    //}
  }
}
