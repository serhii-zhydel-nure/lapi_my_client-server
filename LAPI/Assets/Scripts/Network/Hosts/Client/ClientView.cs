﻿using System;
using UnityEngine;

namespace Network
{
  [RequireComponent(typeof(Client))]
  public class ClientView : MonoBehaviour
  {
    private Client _client;

    private void Awake()
    {
      _client = GetComponent<Client>();
    }

    void OnGUI()
    {
      float lineHeight =Screen.height * 0.05f;

      Rect lableRect =
        new Rect(Screen.width * 0.03f,
          Screen.height * 0.03f,
          Screen.width * 0.1f,
          Screen.height * 0.05f);

      Rect areaRect =
        new Rect(Screen.width * 0.07f,
          Screen.height * 0.03f,
          Screen.width * 0.07f,
          Screen.height * 0.05f);

      if (_client.ConnectionStatus == ConnectionStatus.Disconnected)
      {
        //ip
        GUI.Label(lableRect, "Ip");
        _client.Config.IpAddress = 
          GUI.TextField(areaRect, _client.Config.IpAddress);

        lableRect.y += lineHeight;

        areaRect.y += lineHeight;
        //port
        GUI.Label(lableRect, "Port");
        _client.Config.Port = Int32.Parse(
          GUI.TextField(areaRect, _client.Config.Port.ToString()));

        lableRect.y += lineHeight;
        //port
        if (GUI.Button(lableRect, "Connect client"))
        {
          _client.Connect();
        }
      }

      else
      {
        lableRect.width += Screen.width * 0.1f;
        GUI.Label(lableRect, 
          String.Format("{0} [{1}:{2}]", 
            _client.ConnectionStatus, _client.Config.IpAddress, _client.Config.Port));
        lableRect.width -= Screen.width * 0.1f;

        lableRect.y += lineHeight;
        if(GUI.Button(lableRect, "Disconnect"))
        {
          _client.Disconnect();
        }
      }
    }

  }
}