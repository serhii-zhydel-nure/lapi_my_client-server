﻿using System;
using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
  public enum ConnectionStatus
  {
    Disconnected, Connecting, Connected
  }

  public class Client : AHost
  {
    public ConnectionStatus ConnectionStatus { get { return _connectionStatus; } }
    [SerializeField] [ReadOnly] private ConnectionStatus _connectionStatus;
    public int ConnectionId { get { return _connectionId; } }
    [SerializeField] [ReadOnly] private int _connectionId;


    /// <summary>
    /// int - connectionID
    /// </summary>
    public event Action<int> ConnectedToServer;

    /// <summary>
    /// int - connectionID
    /// </summary>
    public event Action<int> DisconnectedFromServer;

    // Use this for initialization
    public void Connect(bool restartHost = false)
    {
      if (IsHostStarted)
      {
        if (restartHost)
        {
          StartHost();
        }
      }
      else
        StartHost();

      byte error;
      _connectionId = NetworkTransport.Connect(HostId, Config.IpAddress, Config.Port, 0, out error);
      _connectionStatus = ConnectionStatus.Connecting;

      if ((NetworkError)error != NetworkError.Ok)
      {
        Debug.LogFormat("Connection error: {0}", (NetworkError)error);
        _connectionStatus = ConnectionStatus.Disconnected;
      }
    }

    public IEnumerator SendMessages()
    {
      while (true)
      {
        yield return null;

        for (int i = 0; i < UnityEngine.Random.Range(3, 10); i++)
        {
          byte error;
          byte[] message = Encoding.Unicode.GetBytes("Message");
          NetworkTransport.Send(HostId, ConnectionId, ReliableChanel, message, message.Length, out error);

          if ((NetworkError)error != NetworkError.Ok)
            Debug.LogFormat("Error: {0}", (NetworkError)error);
        }
      }
    }

    public void Disconnect(bool stopHost = true)
    {
      if (_connectionStatus == ConnectionStatus.Disconnected)
        return;

      byte error;
      NetworkTransport.Disconnect(HostId, _connectionId, out error);
      _connectionStatus = ConnectionStatus.Disconnected;
      Debug.Log("Disconnected from server.");


      if (stopHost)
        StopHost();
    }

    protected override void AddHost(HostTopology topology)
    {
      HostId = NetworkTransport.AddHost(topology, 0);
    }

    protected override void RemoveHost()
    {
      NetworkTransport.RemoveHost(HostId);
    }

    protected override void OnConnect(MessageData messageData)
    {
      if (messageData.ConnectionId == ConnectionId)
      {
        Debug.Log("Connected to server.");
        _connectionStatus = ConnectionStatus.Connected;

        if (ConnectedToServer != null)
          ConnectedToServer.Invoke(ConnectionId);
      }
      else
        Debug.LogFormat("Other Client {0} connected to this Client.", messageData.ConnectionId);

    }

    protected override void OnDisconnect(MessageData messageData)
    {
      if (messageData.ConnectionId == ConnectionId)
      {
        Debug.LogFormat("Connection to server error: {0}", messageData.Error);

        if (DisconnectedFromServer != null)
          DisconnectedFromServer.Invoke(ConnectionId);

        _connectionStatus = ConnectionStatus.Disconnected;
        Debug.Log("Disconnected from server.");
      }
      else
        Debug.LogFormat("Other Client {0} disconnected from this Client.", messageData.ConnectionId);
    }

    public override void Dispose()
    {
      Disconnect();

      base.Dispose();
    }
  }
}
