﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
  public abstract class AHost : MonoBehaviour, IDisposable
  {
    public HostConfig Config;

    public int HostId { get { return _hostId; } protected set { _hostId = value; } }
    [SerializeField] [ReadOnly] private int _hostId;

    public int ReliableChanel { get { return _reliableChannel; } }
    [SerializeField] [ReadOnly] private int _reliableChannel;

    public int UnReliableChanel { get { return _unReliableChannel; } }
    [SerializeField] [ReadOnly] private int _unReliableChannel;

    public bool IsHostStarted { get { return _isHostStarted; } }
    [SerializeField] [ReadOnly] private bool _isHostStarted = false;

    /// <summary>
    /// int - hostID
    /// </summary>
    public event Action<int> HostStarted;

    /// <summary>
    /// int - hostID
    /// </summary>
    public event Action<int> HostStopped;

    /// <summary>
    /// NetworkEventType.DataEvent received.
    /// </summary>
    public event Action<MessageData> DataMessageReceived = (data)=>{};

    /// <summary>
    /// NetworkEventType.BroadcastEvent received.
    /// </summary>
    public event Action<MessageData> BroadcastMessageReceived = (data)=>{};


    protected void StartHost()
    {
      NetworkTransport.Init();
      ConnectionConfig config = new ConnectionConfig();

      _reliableChannel = config.AddChannel(QosType.Reliable);
      _unReliableChannel = config.AddChannel(QosType.Unreliable);

      HostTopology topology = new HostTopology(config, Config.MaxConnections);

      //null - чтоб все кто хочет могли подключаться
      AddHost(topology);

      if (HostId != default(int) && HostStarted != null)
        HostStarted.Invoke(HostId);

      _isHostStarted = true;
      Debug.LogFormat("{0}", "Host Started");
    }

    protected abstract void AddHost(HostTopology topology);

    protected abstract void RemoveHost();

    protected void StopHost()
    {
      RemoveHost();
      
      if (HostId != default(int) && HostStopped != null)
        HostStopped.Invoke(HostId);

      NetworkTransport.Shutdown();
      _isHostStarted = false;
      Debug.LogFormat("{0}", "Host Stopped");
    }

    private void Update()
    {
      if (!IsHostStarted)
        return;

      byte error;
      int incomingQueueSize = 
        NetworkTransport.GetIncomingMessageQueueSize(HostId, out error);
    
      for (int i = incomingQueueSize; i >= 0; i--)
      {
        MessageData receivedData;

        NetworkEventType networkEvent = ReceiveMessage(out receivedData);

        HandleReceiving(networkEvent, receivedData);
      }
    }

    private NetworkEventType ReceiveMessage(out MessageData data)
    {
      int actualDataSize = Config.DefaultBufferSize;

      NetworkEventType eventType = TryReceiveMessage(out data, ref actualDataSize);

      if (data.Error != NetworkError.Ok)
        if (data.Error == NetworkError.MessageToLong)
        //message to big for allocated buffer
        {
          //allocate bigger buffer
          data = new MessageData(actualDataSize);
          //repeat receiving
          eventType = TryReceiveMessage(out data, ref actualDataSize);
        }
        else
        {
          Debug.LogWarningFormat("Error {0} of receiving message happen.", data.Error);
        }

      return eventType;
    }

    private NetworkEventType TryReceiveMessage(out MessageData data, ref int actualDataSize)
    {
      data = new MessageData(actualDataSize);

      byte error;

      NetworkEventType eventType = NetworkTransport.Receive(
                              out data.HostId,
                              out data.ConnectionId,
                              out data.ChannelId,
                              data.Buffer,
                              data.Buffer.Length,
                              out actualDataSize,
                              out error);

      data.Error = (NetworkError)error;

      return eventType;
    }


    private void HandleReceiving(NetworkEventType eventType, MessageData messageData)
    {
      switch (eventType)
      {
        case NetworkEventType.ConnectEvent:
          OnConnect(messageData);
          break;

        case NetworkEventType.DisconnectEvent:
          OnDisconnect(messageData);
          break;

        case NetworkEventType.DataEvent:
          DataMessageReceived.Invoke(messageData);
          break;

        case NetworkEventType.BroadcastEvent:
          BroadcastMessageReceived.Invoke(messageData);
          break;

        case NetworkEventType.Nothing:
          break;

        default:
          Debug.LogError("Unknown network message type received: " + eventType);
          break;
      }
    }

    protected abstract void OnConnect(MessageData messageData);

    protected abstract void OnDisconnect(MessageData messageData);
   
    public virtual void Dispose()
    {
      if (IsHostStarted)
        StopHost();
    }

    private void OnDestroy()
    {
      Dispose();
    }
  }
}