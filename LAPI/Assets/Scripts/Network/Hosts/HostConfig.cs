﻿using UnityEngine;

namespace Network
{
  [CreateAssetMenu(fileName = "ClientServerConfig", menuName = "ClientServerConfiguration", order = 1)]
  public class HostConfig : ScriptableObject
  {
    public int MaxConnections { get { return _maxConnections; } }
    [Tooltip("Max connections to server.")]
    [SerializeField] private int _maxConnections = 10;

    public string IpAddress { get { return _ipAddress; } set { _ipAddress = value; } }
    [SerializeField] private string _ipAddress = "127.0.0.1";

    public int Port { get { return _port; } set { _port = value; } }
    [SerializeField] private int _port = 5701;


    /// <summary>
    /// How many bytes in default message.
    /// </summary>
    public int DefaultBufferSize { get { return _defaultBufferSize; } }
    [Tooltip("How many bytes in default message")]
    [SerializeField] private int _defaultBufferSize = 256;

  }
}